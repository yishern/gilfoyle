from abc import ABCMeta, abstractmethod
import functools 
import pickle 
import inspect
import os 
from jinja2 import Environment, select_autoescape, FileSystemLoader
import dacite
from .configuration import MicroserviceConfiguration, get_microservice_default_json_config, FrameworkConfiguration, get_framework_default_json_config 
from .logging import Logger

class MicroserviceWrapper:
    def __init__(self, model, microservice_config=None, save_directory='gilfoyle_artifacts'):
        self.model = model
        self.microservice_init(microservice_config)
        self.prepare_artifacts_output(save_directory)
        self.prepare_environment()

    def microservice_init(self, microservice_config):
        updated_microservice_config = get_microservice_default_json_config()
        if microservice_config is not None:
            updated_microservice_config.update(microservice_config)

        self.microservice_config = dacite.from_dict(
            data_class=MicroserviceConfiguration,
            data=updated_microservice_config,
            config=dacite.Config())

    def prepare_artifacts_output(self, directory):
        if os.path.exists(directory):
            assert not os.listdir(directory), f'Path given: `{directory}` is not empty. Please manually delete the files/folders within it' 
        else: 
            os.mkdir(directory)  
        self.save_directory = directory

    def prepare_environment(self):
        env = Environment(
            loader=FileSystemLoader('%s/templates/' % os.path.dirname(__file__)),
            autoescape=select_autoescape(['py']))
        self.web_server_template = env.get_template('run_web_server_template.py')
        self.model_server_template = env.get_template('run_model_server_template.py')

    def render_and_write(self, template, render_items_dict, path):
        rendered_template = template.render(**render_items_dict)
        with open(path, 'w') as f:
            f.write(rendered_template)
        return True

    def serialize(self):
        frame = inspect.stack()[1]
        filename = frame[0].f_code.co_filename 
        basename = filename.replace('.py', '')
        with open(os.path.join(self.save_directory, 'imports.pickle'), 'wb') as f:
            pickle.dump(basename, f, pickle.HIGHEST_PROTOCOL)
        with open(os.path.join(self.save_directory, 'model.pickle'), 'wb') as f:
            pickle.dump(self.model, f, pickle.HIGHEST_PROTOCOL)

        render_items_dict = {
            'import_path': basename, 
            'model_definition_name': self.model.__class__.__name__,
            'model_path': os.path.join(self.save_directory, 'model.pickle'),
            'redis_host': self.microservice_config.redis.host,
            'redis_port': self.microservice_config.redis.port,
            'redis_db': self.microservice_config.redis.db,
            'flask_host': self.microservice_config.flask.host,
            'flask_port': self.microservice_config.flask.port,
            'logging_level': self.microservice_config.logging_level.upper(),
        }

        # TODO: Change path to the immediate directory when call
        rendered_web_server_success = self.render_and_write(
            template=self.web_server_template, 
            render_items_dict=render_items_dict, 
            path=self.microservice_config.naming_convention.web) # 'run_web_server.py'
        rendered_model_server_success = self.render_and_write(
            template=self.model_server_template, 
            render_items_dict=render_items_dict, 
            path=self.microservice_config.naming_convention.model) # 'run_model_server.py'
        return True 

class MicroserviceBackbone(metaclass=ABCMeta):
    def __init__(self, framework_config=None):
        self.dummy = framework_config
        self.framework_init(framework_config)
        # prepare logger
        self.prepare_logger(self.framework_config.logging)

    def framework_init(self, framework_config):
        updated_framework_config = get_framework_default_json_config()
        if framework_config is not None:
            updated_framework_config.update(framework_config)
        self.framework_config = dacite.from_dict(
            data_class=FrameworkConfiguration,
            data=updated_framework_config,  
            config=dacite.Config())

    def prepare_logger(self, logging_config):
        self.logger = Logger(
            config=logging_config,
            name=self.framework_config.name)

    @abstractmethod
    def preprocess_init(self):
        pass 
    @abstractmethod
    def inference_init(self):
        pass

    @abstractmethod
    def preprocess(self):
        pass
        
    @abstractmethod
    def inference(self, x):
        pass 

    def _aux_preprocess(self):
        def wrapper():
            return self.preprocess(self.api_name)
        return wrapper 

    def _aux_inference(self):
        def wrapper():
            return self.inference(self.api_name)
        return wrapper 