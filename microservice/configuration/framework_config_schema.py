from dataclasses import dataclass 

@dataclass
class FrameworkConfiguration:
	name: str
	logging: dict