# Gilfoyle
This repository contains utilities which may be reusable across various machine learning model deployment

## Installation
`pip install -r requirements.txt`

## Pillow installation
### Conda
`conda uninstall --force jpeg libtiff -y
conda install -c conda-foCC="cc -mavx2" pip install --no-cache-dir -U --force-reinstall --no-binary :all: --compile pillow-simd`
If you only care about faster JPEG decompression, it can be `pillo` or `pillow-simd` in the last command above, the latter speeds up other image processing operations.

### Pip
`pip uninstall pillow
CC="cc -mavx2" pip install -U --force-reinstall pillow-simd`