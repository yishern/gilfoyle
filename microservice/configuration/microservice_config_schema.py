from dataclasses import dataclass 
@dataclass
class VersionConfiguration:
	version: str

@dataclass
class RedisConfiguration:
	host: str
	port: int
	db: int

@dataclass
class FlaskConfiguration:
	host: str
	port: int

@dataclass
class OutputNamingConfiguration:
	web: str
	model: str

@dataclass
class MicroserviceConfiguration:
	version: VersionConfiguration
	redis: RedisConfiguration
	flask: FlaskConfiguration
	naming_convention: OutputNamingConfiguration
	logging_level: str