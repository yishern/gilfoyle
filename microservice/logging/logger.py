import logging 
# import logging.config 

class Logger:

	def __init__(self, config, name):
		self.config = config 
		# logging.config.dictConfig(config)
		self.logger = logging.getLogger(name)

	def info(self, message):
		self.logger.info(message)

	def debug(self, message):
		self.logger.debug(message)

	def warning(self, message):
		self.logger.warning(message)

	def error(self, message):
		self.logger.error(message)

	def critical(self, message):
		self.logger.critical(message)

	def exception(self, message):
		self.logger.exception(message)