from .microservice_config_schema import MicroserviceConfiguration
from .framework_config_schema import FrameworkConfiguration
import json 
import os 

def get_json_config(filepath):
	default_config_file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), filepath)
	with open(default_config_file_path, 'r') as f:
		config = json.load(f)
	return config

def get_microservice_default_json_config():
	return get_json_config('default_config/default_microservice_config.json')

def get_framework_default_json_config():
	return get_json_config('default_config/default_framework_config.json')