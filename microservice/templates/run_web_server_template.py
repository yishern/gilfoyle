import flask 
import uuid 
import pickle 
import json
import redis 
import ast
import logging
logging.basicConfig(level=logging.{{logging_level}})
import logging.config 
logging.config.dictConfig({
		"version": 1,
		"formatters": {
			"simple" : {
				"format": "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
			}
		},
		"handlers": {
			"console": {
				"class": "logging.StreamHandler",
				"level": "INFO",
				"formatter": "simple",
				"stream": "ext://sys.stdout"
			}
		},
		"loggers": {
			"sampleLogger": {
				"level": "INFO",
				"handlers": ["console"],
				"propagate": "no"
			}
		},
		"root": {
			"level": "INFO",
			"handlers": ["console"]
		}
})
from {{import_path}} import {{model_definition_name}}

app = flask.Flask(__name__)	
db = redis.StrictRedis(host="{{redis_host}}", port={{redis_port}}, db={{redis_db}})

def load_model():
	global model 
	with open('{{model_path}}', 'rb') as f:
		model = pickle.load(f)
	model.preprocess_init()

@app.route('/')
def homepage():
	return 'Welcome to the API'
	
@app.route('/inference', methods=['POST'])
def inference():
	data = {'success': False}
	try:
		if flask.request.method == 'POST':
			request_data = flask.request.get_json()
			k = str(uuid.uuid4())
			processed_data = model.preprocess(request_data)
			d = {'id': k, **processed_data}
			model.logger.info(f'Request of id: {k} is sent to queue')
			db.rpush('request_queue', json.dumps(d))

			while True:
				output = db.get(k)
				if output is not None:
					output = output.decode("utf-8")
					output = ast.literal_eval(output)
					db.delete(k)
					data = {**data, **output}
					model.logger.info(f'Returning request of id: {k} to client')
					break 
			data['success'] = True
	except Exception as e:
		data['error_message'] = str(e)
	return flask.jsonify(data)


if __name__ == '__main__':
	load_model()
	app.run(host="{{flask_host}}", port={{flask_port}})	